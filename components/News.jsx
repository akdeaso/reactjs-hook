import React, { useState, useEffect } from "react";
import axios from "axios";
import {
  Container,
  Row,
  Col,
  Card,
  Button,
  Form,
  Navbar,
  Nav,
} from "react-bootstrap";

const News = () => {
  const [searchKeyword, setSearchKeyword] = useState("");
  const [articles, setArticles] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const apiKey = import.meta.env.VITE_API_KEY;

  useEffect(() => {
    fetchNews("trending");
  }, []);

  const fetchNews = async (searchKeyword) => {
    const url = `https://newsapi.org/v2/everything?q=${searchKeyword}&apiKey=${apiKey}`;

    try {
      setIsLoading(true);
      const response = await axios.get(url);
      setArticles(response.data.articles);
      setIsLoading(false);
    } catch (error) {
      console.error("Error fetching news:", error);
      setArticles([]);
      setIsLoading(false);
    }
  };

  const handleInputChange = (event) => {
    const searchKeyword = event.target.value;
    setSearchKeyword(searchKeyword);

    if (searchKeyword.trim() === "") {
      fetchNews("trending");
    } else {
      setIsLoading(true);
      fetchNews(searchKeyword);
    }
  };

  return (
    <Container>
      <Navbar bg="light" expand="lg">
        <Navbar.Brand>News App</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto"></Nav>
        </Navbar.Collapse>
      </Navbar>

      <Row className="my-5">
        <Col>
          <Form>
            <Form.Group controlId="searchInput">
              <Form.Control
                type="text"
                placeholder="Search news..."
                value={searchKeyword}
                onChange={handleInputChange}
              />
            </Form.Group>
          </Form>
        </Col>
      </Row>
      <Row>
        <Col>
          {isLoading ? (
            <div className="text-center">
              <div className="spinner-border" role="status">
                <span className="sr-only"></span>
              </div>
            </div>
          ) : articles.length === 0 ? (
            <p>No news found.</p>
          ) : (
            <Row>
              {articles.map((article, index) => (
                <Col md={4} key={index} className="mb-4">
                  <Card>
                    <Card.Img
                      variant="top"
                      src={
                        article.urlToImage || "https://via.placeholder.com/150"
                      }
                      style={{ height: "200px", objectFit: "cover" }}
                    />
                    <Card.Body>
                      <Card.Title>{article.title}</Card.Title>
                      <Card.Text>{article.description}</Card.Text>
                      <Button
                        href={article.url}
                        target="_blank"
                        variant="primary"
                      >
                        Read More
                      </Button>
                    </Card.Body>
                  </Card>
                </Col>
              ))}
            </Row>
          )}
        </Col>
      </Row>
    </Container>
  );
};

export default News;
